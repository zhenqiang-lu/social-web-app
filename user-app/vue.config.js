module.exports = {
    lintOnSave: false,
    devServer: {
        proxy: {  //配置跨域
            '/api': {
                target: 'http://94.74.120.67/api',
                // target: 'http://81.70.35.150:8080/api', 
                // target: 'http://192.168.1.16:8080/api',
                changOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            },
        }
    },
}