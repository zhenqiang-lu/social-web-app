import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login/Login.vue')
  },
  {
    path: '/Login',
    name: 'Login',
    component: () => import('../views/Login/Login.vue')
  },
  {
    path: '/Register',
    name: 'Register',
    component: () => import('../views/Login/Register.vue')
  },
  {
    path: '/LinkManager',
    name: 'LinkManager',
    component: () => import('../views/LinkManager/LinkManager.vue')
  },
  {
    path: '/Help',
    name: 'Help',
    component: () => import('../views/Help/Help.vue')
  },
  {
    path: '/Password',
    name: 'Password',
    component: () => import('../views/Password/Password.vue')
  },
  {
    path: '/EditInfo',
    name: 'EditInfo',
    component: () => import('../views/EditInfo/EditInfo.vue')
  },
  {
    path: '/MySign',
    name: 'MySign',
    component: () => import('../views/EditInfo/MySign.vue')
  },
  {
    path: '/updateLink',
    name: 'updateLink',
    component: () => import('../views/LinkManager/updateLink.vue')
  },
  {
    path: '/Nav',
    name: 'Nav',
    component: () => import('../components/nav/nav.vue'),
    children: [
      {
        path: '/Nav/Index/:userId',
        name: 'Index',
        component: () => import('../views/Index/Index.vue')
      },
      {
        path: '/Nav/My/:userId',
        name: 'My',
        component: () => import('../views/My/My.vue')
      },
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
