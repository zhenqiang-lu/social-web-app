import axios from 'axios'
const request = axios.create({
    baseURL: "/",
    timeout: 30000,
})

//请求拦截器
request.interceptors.request.use(
    config => {
        const token = sessionStorage.getItem('token')
        if(token) {
            config.headers['Authorization'] = `Bearer ${token}`
        }
        return config;
    },
    error => {
        return error.data;
    }
)

//响应拦截器
request.interceptors.response.use(
    response => {
        return response.data;
    },
    error => {
        return error.data;
    }
)

export default request;
