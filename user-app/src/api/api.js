import request from "./axios.js";
import qs from 'qs';

//登录
export const loginPost = (data) => (
  request({
    url: '/api/authenticate',
    method: 'post',
    data,
  })
)
//获取登录账号信息
export const getAccount = () => (
  request({
    url: '/api/account',
    method: 'get',
  })
)
//获取用户昵称等信息
export const getMyInfo = ({ userId }) => {
  return new Promise(async (resolve, reject) => {
    let result = await request({
      url: `/api/user-infos/userId/${userId}`,
      method: 'get',
    })
    result = {
      ...result,
      chatHead: result.chatHead || 'http://94.74.120.67/head.jpg',
      nickName: result.nickName || 'uni有你'
    }
    resolve(result);
  })
}
//注册
export const registerPost = (data) => (
  request({
    url: '/api/register',
    method: 'post',
    data,
  })
)
//查询用户下所有链接
export const getMyLinks = ({ userId }) => (
  request({
    url: `/api/user-link/all/${userId}`,
    method: 'get',
  })
)
//查询用户帮助
export const getAllHelp = (params) => (
  request({
    url: '/api/assistances',
    method: 'get',
    params,
  })
)
//查询所有link
export const getAllLinks = (params) => (
  request({
    url: '/api/links',
    method: 'get',
    params,
  })
)
//忘记密码
export const forgetPwd = ({ username }) => (
  request({
    url: `/api/account/forget/${username}`,
    method: 'get',
  })
)
//修改密码
export const changePwd = (data) => (
  request({
    url: '/api/account/change-password',
    method: 'post',
    data
  })
)
// 编辑用户链接
export const updateUserLink = (data) => (
  request({
    url: `/api/user-links/${data.id}`,
    method: 'put',
    data,
  })
)

// 创建用户链接
export const createUserLink = (data) => (
  request({
    url: '/api/user-links',
    method: 'post',
    data,
  })
)
//删除用户链接
export const delUserLink = (data) => (
  request({
    url: `/api/user-links/${data.id}`,
    method: 'delete',
  })
)
// 修改用户信息
export const updateUserInfo = (data) => (
  request({
    url: `/api/user-infos/${data.id}`,
    method: 'put',
    data,
  })
)