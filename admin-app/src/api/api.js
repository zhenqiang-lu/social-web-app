import request from "./axios.js";

//登录
export const loginPost = (data) => (
  request({
    url: '/api/authenticate',
    method: 'post',
    data,
  })
)
//获取登录账号信息
export const getAccount = () => (
  request({
    url: '/api/account',
    method: 'get',
  })
)
//链接管理获取所有链接
export const getAllLinksPost = (params) => (
  request({
    url: '/api/links',
    method: 'get',
    params,
  })
)

//删除链接
export const deleteLink = (id) => (
  request({
    url: `/api/links/${id}`,
    method: 'delete',
  })
)

//更新链接
export const updateLink = (data) => (
  request({
    url: `/api/links/${data.id}`,
    method: 'put',
    data
  })
)
//新增链接
export const addLink = (data) => (
  request({
    url: `/api/links`,
    method: 'post',
    data
  })
)
//查询所有帮助链接
export const getAllAssistances = (params) => (
  request({
    url: '/api/assistances',
    method: 'get',
    params,
  })
)
//删除帮助链接
export const delAssistances = (id) => (
  request({
    url: `/api/assistances/${id}`,
    method: 'delete',
  })
)
//创建帮助链接
export const createAssistances = (data) => (
  request({
    url: `/api/assistances`,
    method: 'post',
    data
  })
)

//创建帮助链接
export const updateAssistances = (data) => (
  request({
    url: `/api/assistances/${data.id}`,
    method: 'put',
    data
  })
)
//获取所有admin
export const getAllAdmin = (params) => (
  request({
    url: `/api/admin/users/admin`,
    method: 'get',
    params
  })
)
//获取所有admin
export const getAllCustomer = (params) => (
  request({
    url: `/api/admin/users/customer`,
    method: 'get',
    params
  })
)
//删除用户
export const delUser = (login) => (
  request({
    url: `/api/admin/users/${login}`,
    method: 'delete',
  })
)
//创建用户
export const createdUser = (data) => (
  request({
    url: `/api/admin/users`,
    method: 'post',
    data
  })
)
//编辑用户
export const updateUser = (data) => (
  request({
    url: `/api/admin/users`,
    method: 'put',
    data
  })
)
//reset password
export const resetUser = (id) => (
  request({
    url: `/api/admin/users/reset/${id}`,
    method: 'post',
  })
)

//获取用户昵称等信息
export const getUserInfo = (userId) => (
  request({
    url: `/api/user-infos/userId/${userId}`,
    method: 'get',
  })
)