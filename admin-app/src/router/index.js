import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login/Login.vue')
  },
  {
    path: '/Nav',
    name: 'Nav',
    component: () => import('../components/nav/nav.vue'),
    children: [
      {
        path: '/Nav/Href',
        name: 'Href',
        component: () => import('../views/Href/Href.vue')
      },
      {
        path: '/Nav/Account',
        name: 'Account',
        component: () => import('../views/Account/Account.vue')
      },
      {
        path: '/Nav/Help',
        name: 'Help',
        component: () => import('../views/Help/Help.vue')
      },
      {
        path: '/Nav/Setting',
        name: 'Setting',
        component: () => import('../views/Setting/Setting.vue')
      },
    ]
  },
  
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
